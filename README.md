Country Alter UI
=======================

Provides an interface to alter the built in country names and codes.

Installation
------------
Install the Country Alter UI module as you would normally install a contributed Drupal module.
Visit [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules) for further information.

Configuration
-------------
1. Go to the Drupal admin interface and navigate to
   `admin/config/regional/countries`.
2. Add the country entries in the text field. Each one should be in its own line.
   The entry will either be added or if the key is the same, it will be overridden.
