<?php

namespace Drupal\country_alter_ui\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Locale\CountryManagerInterface;

/**
 * Implements CountryAlterSettingsForm class.
 *
 * @package Drupal\country_alter_ui\Form
 */
class CountryAlterSettingsForm extends ConfigFormBase {

  /**
   * The country manager.
   *
   * @var \Drupal\Core\Locale\CountryManagerInterface
   */
  protected $countryManager;

  /**
   * CountryAlterSettingsForm constructor.
   *
   * @param \Drupal\Core\Locale\CountryManagerInterface $country_manager
   *   The country manager.
   */
  public function __construct(CountryManagerInterface $country_manager) {
    $this->countryManager = $country_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('country_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'country_alter_ui_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['country_alter_ui.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('country_alter_ui.settings');
    $countries = $config->get('countries') ?? [];

    $form['sort_alphabetically'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Sort Countries Alphabetically'),
      '#default_value' => $config->get('sort_alphabetically') ?? FALSE,
    ];

    $form['countries'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Countries'),
      '#description' => $this->t('Enter one country per line in the format: code|name.'),
      '#default_value' => implode(PHP_EOL, $countries),
    ];

    $all_countries = $this->countryManager::getStandardList();

    $form['filter_countries'] = [
      '#type' => 'select',
      '#title' => $this->t('Filter Countries'),
      '#description' => $this->t('Select the countries to filter out.'),
      '#multiple' => TRUE,
      '#options' => $all_countries,
      '#default_value' => $config->get('filter_countries') ?? [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $countries = array_filter(array_map('trim', explode(PHP_EOL, $form_state->getValue('countries'))));

    foreach ($countries as $country) {
      $parts = explode('|', $country, 2);
      if (count($parts) !== 2) {
        $form_state->setErrorByName('countries', $this->t('Invalid format. Enter one country per line in the format: code|name.'));
        break;
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $countries = array_filter(array_map('trim', explode(PHP_EOL, $form_state->getValue('countries'))));

    $config = $this->config('country_alter_ui.settings');
    $config->set('countries', $countries);
    $config->set('filter_countries', $form_state->getValue('filter_countries'));
    $config->set('sort_alphabetically', $form_state->getValue('sort_alphabetically'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
